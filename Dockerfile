FROM python:3.9-slim-bullseye

RUN python -m pip install --upgrade pip
RUN pip install python-dotenv paho-mqtt pyserial

ADD app.py .

CMD ["python", "./app.py"]