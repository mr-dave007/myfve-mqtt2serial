import json
import serial
import logging
import os
import random
from paho.mqtt import client as mqtt_client
from dotenv import load_dotenv
import threading as th

topic = "myfve/inverter/"
data = {
    "ppv": 0
}


class RepeatTimer(th.Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            logging.info("Connected to MQTT Broker!")
        else:
            logging.error("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(f'myfve-mqtt2serial-{random.randint(0, 1000)}')
    client.username_pw_set(os.environ["MQTT_USERNAME"], os.environ["MQTT_PASSWORD"])
    client.on_connect = on_connect
    client.connect(host=os.environ["MQTT_HOST"], port=int(os.environ["MQTT_PORT"]), keepalive=60)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        logging.info(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
        data_message = json.loads(str(msg.payload.decode("utf-8")))
        if msg.topic == topic + os.environ["INVERTER_IP"] + "/status/ppv":
            data["ppv"] = data_message["ppv"]

    client.subscribe([(topic + os.environ["INVERTER_IP"] + "/status/ppv", 0)])
    client.on_message = on_message


def send2serial():
    logging.info("Send Data " + str(data["ppv"]) + " to Serial port " + os.environ["SERIAL_PORT"])
    with serial.Serial(os.environ["SERIAL_PORT"], 115200, timeout=1) as ser:
        ser.write(json.dumps(data, default=str))


def run():
    client = connect_mqtt()
    subscribe(client)
    serial_timer = RepeatTimer(int(os.environ["DELAY2SEND"]), send2serial)
    serial_timer.start()
    try:
        client.loop_forever()
    except KeyboardInterrupt:
        serial_timer.cancel()
        exit(0)


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    logging.info("Starting MyFVE-MQTT2Serial app")
    load_dotenv()
    if os.environ["DELAY2SEND"] is None:
        logging.error("Missing variable DELAY2SEND")
        exit(2)
    if os.environ["MQTT_HOST"] is None:
        logging.error("Missing variable MQTT_HOST")
        exit(2)
    if os.environ["MQTT_PORT"] is None:
        logging.error("Missing variable MQTT_PORT")
        exit(2)
    if os.environ["MQTT_USERNAME"] is None:
        logging.error("Missing variable MQTT_USERNAME")
        exit(2)
    if os.environ["MQTT_PASSWORD"] is None:
        logging.error("Missing variable MQTT_PASSWORD")
        exit(2)
    if os.environ["INVERTER_IP"] is None:
        logging.error("Missing variable INVERTER_IP")
        exit(2)
    if os.environ["SERIAL_PORT"] is None:
        logging.error("Missing variable SERIAL_PORT")
        exit(2)
    run()
